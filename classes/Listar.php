<?php

require_once 'classes/Conexao.php';

class Listar
{
	public function listar()
    {
        $query = "SELECT * FROM t_pessoa";
        $conexao = Conexao::pegarConexao();
        $resultado = $conexao->query($query);
        $lista = $resultado->fetchAll();
        return $lista;
    }


}